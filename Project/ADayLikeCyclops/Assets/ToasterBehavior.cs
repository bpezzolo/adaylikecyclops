﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Spine.Unity;
using SeventyOneSquared;

public class ToasterBehavior : MonoBehaviour {

    public int N_TOASTS = 0;
    public Vector2 MIN_IMPULSE = new Vector2(0, 0);
    public Vector2 MAX_IMPULSE = new Vector2(0, 0);

    public GameObject TOAST_PREFAB;
    public GameObject SMOKE_EMITTER_PREFAB;

    private List<Vector3> spawns;
    private int currentSpawnIndex;

    private SkeletonAnimation skeletonAnimation;

    private MiniGameManager miniGameManager;

    private float elapsedTime;

    private float maxFinalDelay = 1.0f;

    private int deadCharacters;

    void Awake ()
    {
        spawns = new List<Vector3>();
        currentSpawnIndex = 0;

        elapsedTime = 0f;

        deadCharacters = 0;
    }

    // Use this for initialization
    void Start ()
    {
        miniGameManager = FindObjectOfType(typeof(MiniGameManager)) as MiniGameManager;

        skeletonAnimation = GetComponent<SkeletonAnimation>();

        ScheduleSpawns();
    }
	
	// Update is called once per frame
	void Update ()
    {
        elapsedTime += Time.deltaTime;
        if ((currentSpawnIndex < N_TOASTS) && (elapsedTime >= spawns[currentSpawnIndex].z))
        {
            skeletonAnimation.state.SetAnimation(0, "Shoot", false);
            SpawnCharacter(spawns[currentSpawnIndex]);

            currentSpawnIndex += 1;
        }
    }

    private void ScheduleSpawns()
    {
        for (int i = 0; i < N_TOASTS; ++i)
        {
            float spawnTime = Random.Range(0, miniGameManager.DURATION - maxFinalDelay);
            Vector3 spawnParams = new Vector3(Random.Range(MIN_IMPULSE.x, MAX_IMPULSE.x), Random.Range(MIN_IMPULSE.y, MAX_IMPULSE.y), spawnTime);
            spawns.Add(spawnParams);
            
            spawns.Sort(CompareSpawns);
        }
        
    }

    private int CompareSpawns(Vector3 a, Vector3 b)
    {
        return (int) (a.z - b.z);
    }

    private void SpawnCharacter(Vector3 spawnData)
    {
        Transform thisTransform = this.gameObject.transform;
        GameObject toast = Instantiate(TOAST_PREFAB) as GameObject;
        toast.transform.parent = thisTransform;
        toast.transform.position = new Vector3(thisTransform.position.x, thisTransform.position.y, thisTransform.position.z + 2);

        GameObject anchor = toast.transform.Find("EmissionAnchor").gameObject;

        GameObject emitterObject = Instantiate(SMOKE_EMITTER_PREFAB) as GameObject;

        emitterObject.transform.position = new Vector3(anchor.transform.position.x, anchor.transform.position.y+5, toast.transform.position.z + 2);

        PDUnity emitter = emitterObject.GetComponent<PDUnity>();
        emitter.emitterOrigin = anchor;

        EmissionAnchorScript anchorScrip = anchor.GetComponent<EmissionAnchorScript>();
        anchorScrip.EMITTER = emitterObject;

        Rigidbody toastBody = toast.GetComponent<Rigidbody>();
        toastBody.AddForce(new Vector3(spawnData.x, spawnData.y, 0));

        float torque = Random.Range(-100, 100);
        toastBody.AddTorque(new Vector3(0, 0, torque), ForceMode.Impulse);
    }

    public void CharacterDied(GameObject character)
    {
        deadCharacters += 1;
        if (deadCharacters >= N_TOASTS)
        {
            miniGameManager.EndGame(true);
        }
    }
}
