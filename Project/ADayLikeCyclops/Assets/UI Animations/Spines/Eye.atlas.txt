
Eye.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
UI/eyeball
  rotate: false
  xy: 2, 166
  size: 298, 298
  orig: 298, 298
  offset: 0, 0
  index: -1
UI/iris
  rotate: false
  xy: 2, 2
  size: 162, 162
  orig: 162, 162
  offset: 0, 0
  index: -1
UI/shine
  rotate: false
  xy: 582, 410
  size: 54, 54
  orig: 54, 54
  offset: 0, 0
  index: -1
UI/splat
  rotate: false
  xy: 302, 186
  size: 278, 278
  orig: 278, 278
  offset: 0, 0
  index: -1
