﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour {
    public Button yourButton;
    public string sceneToGo;

    void Start()
    {
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        if (sceneToGo == "Quit")
        {
            Application.Quit(); 
        }
        else
        {
            SceneManager.LoadScene(sceneToGo, LoadSceneMode.Single);
        }
    }
}
