
Character.png
size: 512,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Character/body_back
  rotate: false
  xy: 2, 70
  size: 167, 180
  orig: 167, 180
  offset: 0, 0
  index: -1
Character/body_front
  rotate: false
  xy: 171, 76
  size: 164, 174
  orig: 164, 174
  offset: 0, 0
  index: -1
Character/eyes
  rotate: false
  xy: 337, 84
  size: 60, 20
  orig: 60, 20
  offset: 0, 0
  index: -1
Character/mouth
  rotate: false
  xy: 2, 2
  size: 17, 66
  orig: 17, 66
  offset: 0, 0
  index: -1
Character/shadow
  rotate: false
  xy: 337, 106
  size: 135, 144
  orig: 135, 144
  offset: 0, 0
  index: -1
