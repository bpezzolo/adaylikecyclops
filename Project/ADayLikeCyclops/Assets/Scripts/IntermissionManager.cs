﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntermissionManager : MonoBehaviour
{

    // Balance Constants
    public int lives = 4;
    public int score = 0;
    public int speed = 1;
    public int difficulty = 1;
    public string[] minigamesScenes;
    private string lastMinigame = "";

    //----
    public GameObject canvas = null;
    public GameObject animationStart = null;
    public GameObject animationScore = null;
    public GameObject animationLevelUp = null;
    public GameObject animationSpeedUp = null;
    public GameObject animationLoseLife = null;
    public GameObject animationGameOver = null;

    //-----------------------------------------------------------------------

    // Main Controller
    public IEnumerator PlayIntermission(GameObject animationName, float animationDuration, bool reduceLife, string animationText)
    {
        GameObject animation = Instantiate(animationName) as GameObject;
        animation.transform.SetParent(canvas.transform, false);
        animation.GetComponent<RectTransform>().localPosition = Vector2.zero;
        //animation.GetComponent<IntermissionAnimation>().animationToPlay = animationName;
        animation.GetComponent<IntermissionAnimation>().optionalText.GetComponent<Text>().text = animationText;
        animation.GetComponent<IntermissionAnimation>().lifeAmount = lives;
        if (reduceLife) {
            animation.GetComponent<IntermissionAnimation>().lifeToReduce = lives+1;
        }
        
        yield return new WaitForSeconds(1);
        if (lastMinigame != "") { DestroyLastMinigame(); }
        
        if (lives > 0) {
            yield return new WaitForSeconds(animationDuration - 1.0f);
            GetNextMinigame();
        } else {
            yield return new WaitForSeconds(5.0f);
            EndGame();
        };
    }

    // Functions
    public void EndMinigame(bool victory)
    {
        if (victory) { GetScore(); } else { LoseLife(); }
    }

    public void LoseLife()
    {
        lives--;
        if (lives >= 1)
        {
            StartCoroutine(PlayIntermission(animationLoseLife, 5.0f,true,"OH NO!"));
        }
        else {
            StartCoroutine(PlayIntermission(animationGameOver, 5.0f,true,"GAME OVER")); // tratar diferente no caso de gameover
        };
    }

    public void GetScore()
    {
        score++;
        if ((score == 10) || (score == 30)) { speed++; StartCoroutine(PlayIntermission(animationSpeedUp, 5.0f,false,"FASTER")); }
        else if ((score == 20) || (score == 40)) { difficulty++; StartCoroutine(PlayIntermission(animationLevelUp, 5.0f,false,"HARDER")); }
        else StartCoroutine(PlayIntermission(animationScore, 5.0f,false,"OK"));
    }

    public void DestroyLastMinigame()
    {
        SceneManager.UnloadSceneAsync(lastMinigame);
    }

    public void GetNextMinigame()
    {
        int idSorted = Random.Range(0, minigamesScenes.Length);
        while (minigamesScenes[idSorted] == lastMinigame)
        {
            idSorted = Random.Range(0, minigamesScenes.Length);
        }
        SceneManager.LoadScene(minigamesScenes[idSorted], LoadSceneMode.Additive);
        lastMinigame = minigamesScenes[idSorted];
    }

    public void EndGame()
    {
        SceneManager.LoadScene("HomeScreen", LoadSceneMode.Single);
    }

    // Debugger
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetNextMinigame();
        }
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            DestroyLastMinigame();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoseLife();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            GetScore();
        }
    }

    // Get Ready
    public void Start() {
        StartCoroutine(PlayIntermission(animationStart, 5.0F, false, "GET READY"));
    }
}