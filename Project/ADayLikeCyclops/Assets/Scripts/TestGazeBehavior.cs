﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGazeBehavior : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        GazeTimeCounter.OnTimeout += Timeout;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void Timeout(GameObject self)
    {
        if (this.gameObject.Equals(self))
            Debug.Log("TIMEOUT FELIZ");
        else
            Debug.Log("INFELIZ");
    }
}
