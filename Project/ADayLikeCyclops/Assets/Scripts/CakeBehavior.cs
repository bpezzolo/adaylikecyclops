﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CakeBehavior : MonoBehaviour
{
    public Vector3 FINAL_POSITION;

    public Animator handAnimator;

    private GazeTimeCounter gazeCake;  

    private MiniGameManager miniGameManager;

    private Camera mainCamera;

    private float movementPacing;
    private Vector3 startPosition;
    private float currentProgress;
    private bool isHandUp = false;

    private bool shouldUpdate = true;

    void Awake()
    {
        currentProgress = 0f;
    }

	// Use this for initialization
	void Start ()
    {
        miniGameManager = FindObjectOfType(typeof(MiniGameManager)) as MiniGameManager;
        if(!miniGameManager)
        {
            Debug.LogError("Where is the miniGameManager?");
        }

        gazeCake = GetComponentInChildren<GazeTimeCounter>() as GazeTimeCounter;

        GazeTimeCounter.OnTimeout += Timeout;

        mainCamera = Camera.main;
        startPosition = mainCamera.transform.position;

        currentProgress = gazeCake.GetProgress();

        MiniGameManager.OnEndGame += Caught;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(shouldUpdate)
        {
            float newProgress = gazeCake.GetProgress();
            if (currentProgress == newProgress)
            {
                if (isHandUp)
                {
                    isHandUp = false;
                    handAnimator.SetTrigger("moveDown");
                }
            }
            else
            {
                currentProgress = newProgress;
                if (!isHandUp)
                {
                    handAnimator.SetTrigger("moveUp");
                    isHandUp = true;
                }
                mainCamera.gameObject.transform.position = Vector3.Lerp(startPosition, FINAL_POSITION, currentProgress);
            }
        }
	}

    void OnDestroy()
    {
        GazeTimeCounter.OnTimeout -= Timeout;
        MiniGameManager.OnEndGame -= Caught;
    }


    private void Timeout(GameObject self)
    {
        if(self.Equals(this.gameObject))
        {
            shouldUpdate = false;
            handAnimator.SetBool("win", true);
            miniGameManager.EndGame(true);
        }
    }

    private void Caught(bool win)
    {
        shouldUpdate = false;
        if (!win) handAnimator.SetBool("lose", true);
    }
}
