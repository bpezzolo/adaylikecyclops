﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.EyeTracking;

public class CyclopsGazeController : MonoBehaviour
{
    public float MOVING_SPEED = 5f;
    public float VISUALIZATION_DISTANCE = 10f;
    public float THRESHOLD = 0.5f;

    private GazePoint lastGazePoint = GazePoint.Invalid;

    // Members used for gaze bubble (filtered gaze visualization):
    private bool _hasHistoricPoint;
    private Vector3 _historicPoint;

    [Range(0.1f, 1.0f), Tooltip("How heavy filtering to apply to gaze point bubble movements. 0.1f is most responsive, 1.0f is least responsive.")]
    public float FilterSmoothingFactor = 0.15f;

    private MiniGameManager miniGameManager;


    // Use this for initialization
    void Start()
    {
        Vector3 position = transform.position;
        position.z = VISUALIZATION_DISTANCE;
        transform.position = position;

        GazePoint gazePoint = EyeTracking.GetGazePoint();
        if (gazePoint.IsWithinScreenBounds)
        {
            lastGazePoint = gazePoint;
        }
        else
        {
            transform.position = (Smoothify(ProjectToPlaneInWorld(lastGazePoint)));
        }
       
    }

    // Update is called once per frame
    void Update()
    {
    }

    void LateUpdate()
    {
        Vector3 targetPosition = (Smoothify(ProjectToPlaneInWorld(lastGazePoint)));
        if (Mathf.Abs((targetPosition.y - transform.position.y)) <= THRESHOLD) targetPosition.y = transform.position.y;

        transform.position += (targetPosition - transform.position) * MOVING_SPEED * Time.deltaTime;

        GazePoint gazePoint = EyeTracking.GetGazePoint();

        if (gazePoint.SequentialId > lastGazePoint.SequentialId && gazePoint.IsWithinScreenBounds)
        {
            lastGazePoint = gazePoint;
        }
    }

    private Vector3 ProjectToPlaneInWorld(GazePoint gazePoint)
    {
        Vector3 gazeOnScreen = gazePoint.Screen;
        gazeOnScreen += (transform.forward * VISUALIZATION_DISTANCE);
        return Camera.main.ScreenToWorldPoint(gazeOnScreen);
    }

    private Vector3 Smoothify(Vector3 point)
    {
        if (!_hasHistoricPoint)
        {
            _historicPoint = point;
            _hasHistoricPoint = true;
        }

        var smoothedPoint = new Vector3(
            point.x * (1.0f - FilterSmoothingFactor) + _historicPoint.x * FilterSmoothingFactor,
            point.y * (1.0f - FilterSmoothingFactor) + _historicPoint.y * FilterSmoothingFactor,
            point.z * (1.0f - FilterSmoothingFactor) + _historicPoint.z * FilterSmoothingFactor);

        _historicPoint = smoothedPoint;

        return smoothedPoint;
    }
}
