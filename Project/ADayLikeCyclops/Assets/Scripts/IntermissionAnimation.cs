﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using Spine;

public class IntermissionAnimation : MonoBehaviour {
    public GameObject chronometer;
    public GameObject scoreText;
    public GameObject optionalText;
    public GameObject bg;
    public float timer = 5;
    private float maxtimer;
    private float alpha = 0;
    private IntermissionManager manager = null;
    public GameObject[] lives;
    public int lifeAmount;
    public int lifeToReduce;

    public void Start()
    {
        StartCoroutine(MainSequence());
        if ((lifeToReduce != 0) && (lifeAmount > 0))
        {
            lifeAmount++;
            UpdateLives();
        }
        else
        {
            UpdateLives();
        }
        maxtimer = timer;
    }

    public IEnumerator MainSequence()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<IntermissionManager>();
        yield return new WaitForSeconds(1.5f);
        if ((lifeToReduce != 0) && (lifeAmount > 0))
        {
            lifeAmount--;
            AnimateLife(lifeToReduce);
        }
        yield return new WaitForSeconds(1.0f);
        optionalText.SetActive(false);
        scoreText.SetActive(true);
        scoreText.GetComponent<Text>().text = manager.score.ToString("0");
    }

    public void UpdateLives()
    {
        if (lifeAmount == 3)
        {
            lives[3].SetActive(false);
        }
        else if (lifeAmount == 2)
        {
            lives[2].SetActive(false);
            lives[3].SetActive(false);
        }
        else if (lifeAmount == 1)
        { 
            lives[1].SetActive(false);
            lives[2].SetActive(false);
            lives[3].SetActive(false);
        }
        else if (lifeAmount == 0)
        {
            lives[0].SetActive(false);
            lives[1].SetActive(false);
            lives[2].SetActive(false);
            lives[3].SetActive(false);
        }
    }

    public void AnimateLife(int lifeToReduce)
    {
        lives[lifeToReduce - 1].GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "dying", false);
    }

    public void FadeInAndOut()
    {
        for (int i = 0; i < lives.Length; i++) {
            lives[i].GetComponent<SkeletonGraphic>().material.color = new Color(1, 1, 1, alpha/2);
        }
        timer -= Time.deltaTime;
        if ((timer > maxtimer-0.5) && (timer < maxtimer)) {
            alpha += Time.deltaTime * 4;
            
        }
        if (timer < 0.5) { alpha -= Time.deltaTime * 4;
        }
        bg.GetComponent<Image>().material.color = new Color(1, 1, 1, alpha);
    }

    void Update () {
        if (timer > 0) {
            FadeInAndOut();
        }
        else {
            chronometer.GetComponent<Text>().text = "";
            GameObject.Destroy(this.gameObject);
        }
    }
}
