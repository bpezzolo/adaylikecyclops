﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBehavior : MonoBehaviour
{
    public Slider slider;
    public GameObject explosionIcon, bombIcon;

    private bool finished = true;

	// Use this for initialization
	void Start ()
    {
        MiniGameManager.OnEndGame += Timeout;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(!finished)
        {
            CheckForEnd(false);
        }
	}

    public void CheckForEnd(bool forceValue)
    {
        float sliderValue = slider.value;

        if (forceValue || sliderValue == 1)
        {
            bombIcon.SetActive(false);
            explosionIcon.SetActive(true);
            finished = true;
        }
    }

    private void Timeout(bool win)
    {
        finished = true;

        CheckForEnd(true);
    }
}
