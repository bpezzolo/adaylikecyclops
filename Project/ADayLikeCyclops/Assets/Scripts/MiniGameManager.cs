﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MiniGameManager : MonoBehaviour {

    public float DURATION = 10f;
    public bool LOSE_ON_TIMEOUT = false;
    public bool CHANGES_SCENE_ON_TIMEOUT = true;

    public Slider timeSlider;
    public Text timeText;

    public delegate void GameEndAction(bool win);
    public static event GameEndAction OnEndGame;

    private float timer;

    private string intermissionScene = "BaseScene_MiniGame";

    private bool didWin = false;

    private bool hasFinished = false;

    void Awake()
    {
        timer = DURATION;
    }

	// Use this for initialization
	void Start ()
    {
        if(!hasFinished)
        {
            UpdateTimer();
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!hasFinished)
        {
            UpdateTimer();

            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                EndGame(!LOSE_ON_TIMEOUT);
            }
        }
    }

    public void EndGame(bool win)
    {
        hasFinished = true;

        didWin = win;
        if (OnEndGame != null)
        {
            OnEndGame(didWin);
        }

        if (CHANGES_SCENE_ON_TIMEOUT)
        {
            ChangeScene(win);
        }
    }

    public void ChangeScene(bool win)
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<IntermissionManager>().EndMinigame(win);
    }

    private void UpdateTimer()
    {
        timeText.text = timer.ToString() + "s";
        timeSlider.value = (DURATION - timer) / DURATION;
    }
}
