﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    private static AudioManager instance;

    private AudioManager() { }

    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new AudioManager();
            }
            return instance;
        }
    }

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public AudioSource currentSFX;
    public AudioSource currentBGM;

    public void PlaySoundFX(AudioClip audio, bool highPriority = true, float delay = 0)
    {
        if(highPriority)
        {
            if (!currentSFX.clip.Equals(audio))
            {
                currentSFX.clip = audio;
            }
            if (delay != 0)
                currentSFX.PlayDelayed(delay);
            else
                currentSFX.Play();
        }
        else
        if(!currentSFX.isPlaying)
        {
            currentSFX.clip = audio;
            if (delay != 0)
                currentSFX.PlayDelayed(delay);
            else
                currentSFX.Play();
        }        
    }

    public void PlayBGMusic(AudioClip clip)
    {
        if (currentBGM.isPlaying)
        {
            currentBGM.Stop();
        }

        currentBGM.clip = clip;

        currentBGM.Play();
    }
}
