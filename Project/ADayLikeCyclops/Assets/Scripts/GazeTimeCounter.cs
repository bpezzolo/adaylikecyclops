﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.EyeTracking;

[RequireComponent(typeof(GazeAware))]
[RequireComponent(typeof(Collider))]
public class GazeTimeCounter : MonoBehaviour
{
    public float DURATION = 5f;
    public float INITIAL_TIME = 0f;
    public float TIME_INCREMENT_RATE = 0f;
    public float GAZE_INCREMENT_RATE = 0f;

    public bool IS_CUMMULATIVE = false;
    public bool USE_TIMEUP = false;

    public delegate void TimeoutAction(GameObject self);
    public static event TimeoutAction OnTimeout;
    public static event TimeoutAction OnTimeup;

    private float timer;
    private bool finished = false;

    private GazeAware gazeAware;

    void Awake()
    {
        timer = INITIAL_TIME;
    }

    // Use this for initialization
    void Start()
    {
        gazeAware = GetComponent<GazeAware>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!finished)
        {
            float deltaTime = Time.deltaTime;
            if (gazeAware.HasGazeFocus)
            {
                timer += GAZE_INCREMENT_RATE * deltaTime;
            }
            else if (!IS_CUMMULATIVE)
            {
                timer = 0f;
            }

            timer += TIME_INCREMENT_RATE * deltaTime;

            if (timer >= DURATION)
            {
                finished = true;

                if(OnTimeout != null) OnTimeout(this.gameObject);
            }
            else if (USE_TIMEUP && timer <= 0)
            {
                finished = true;

                if (OnTimeup != null) OnTimeup(this.gameObject);
            }
        }
    }

    public float GetProgress()
    {
        return (timer / DURATION);
    }
}
