﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.EyeTracking;

public class JeanBehavior : MonoBehaviour
{
    public float PREPARE_TIME = 0.5f;

    public Vector2[] INTERRUPTIONS_TIME;
    // x == duration
    // y == agenda or start time

    public Animator jeanSkeleton;

    public GazeAware cakeAware;

    public float FOCUSED_TIME = 1f;

    private int nInterruptions;

    private MiniGameManager miniGameManager;

    private List<Vector2> interruptionsList;

    public enum JeanState { NOT_WATCHING, ALMOST_WATCHING, WATCHING, FINISHED };
    private JeanState currentState;

    private int currentAgenda;
    private float currentTime;
    private float currentPrepareTime;

    private float hasEnoughFocusTimer;

    void Awake()
    {
        nInterruptions = INTERRUPTIONS_TIME.Length;
        currentState = JeanState.NOT_WATCHING;

        currentAgenda       = 0;
        currentTime         = 0f;
        currentPrepareTime  = 0f;
        hasEnoughFocusTimer = 0f;
    }

	// Use this for initialization
	void Start ()
    {
        miniGameManager = FindObjectOfType(typeof(MiniGameManager)) as MiniGameManager;
        if (!miniGameManager)
        {
            Debug.LogError("Where is the miniGameManager?");
        }

        FillAgenda();
    }
	
	// Update is called once per frame
	void Update ()
    {
        StateMachine();
    }

    void FillAgenda()
    {
        for(int i = 0; i < nInterruptions; ++i )
        {
            float agenda = Random.Range(0, miniGameManager.DURATION - INTERRUPTIONS_TIME[i].x);
            INTERRUPTIONS_TIME[i].y = agenda;
        }

        interruptionsList = new List<Vector2>(INTERRUPTIONS_TIME);

        interruptionsList.Sort(CompareAgendas);
    }

    int CompareAgendas(Vector2 a, Vector2 b)
    {
        return (int) (a.y - b.y);
    }

    void StateMachine()
    {
        switch(currentState)
        {
            case JeanState.NOT_WATCHING:

                currentTime += Time.deltaTime;
                if ( currentTime >= interruptionsList[currentAgenda].y)
                {
                    PrepareToWatch();
                }

            break;

            case JeanState.ALMOST_WATCHING:

                currentPrepareTime += Time.deltaTime;
                if (currentPrepareTime >= PREPARE_TIME)
                {
                    jeanSkeleton.SetTrigger("startLooking");
                    currentState = JeanState.WATCHING;
                }

            break;

            case JeanState.WATCHING:

                float deltaTime = Time.deltaTime;

                if (cakeAware.HasGazeFocus)
                {
                    hasEnoughFocusTimer += deltaTime;
                    if(hasEnoughFocusTimer >= FOCUSED_TIME)
                    {
                        miniGameManager.EndGame(false);
                        currentState = JeanState.FINISHED;
                    }                    
                }
                else hasEnoughFocusTimer = 0f;

                currentTime += deltaTime;
                if (currentTime >= interruptionsList[currentAgenda].x)
                {
                    currentTime = 0f;
                    currentPrepareTime = 0f;
                    hasEnoughFocusTimer = 0f;

                    jeanSkeleton.SetTrigger("stopLooking");

                    if (++currentAgenda >= nInterruptions)
                        currentState = JeanState.FINISHED;
                    else
                    {
                        currentState = JeanState.NOT_WATCHING;
                    }
                }

            break;

            case JeanState.FINISHED:
            break;

            default:
                return;
        }
    }

    void PrepareToWatch()
    {
        currentState = JeanState.ALMOST_WATCHING;
        currentTime = 0;
        currentPrepareTime = 0;
        jeanSkeleton.SetTrigger("freakOut");
    }
}
