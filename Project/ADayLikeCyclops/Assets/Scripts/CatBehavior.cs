﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.EyeTracking;

public class CatBehavior : MonoBehaviour
{
    public float MOVING_SPEED = 5f;
    public float CAT_MAX_Y_OFFSET = 10f;
    public float CAT_MIN_Y_OFFSET = 2f;

    public float VISUALIZATION_DISTANCE = 10f;
    public float THRESHOLD = 0.5f;

    public float LOOKING_TIME = 5f;
    public float PREP_TIME = 0.5f;
    public float ATTACK_TIME = 0.5f;
    public float AT_PLACE_TIME = 0.5f;

    private GazePoint lastGazePoint = GazePoint.Invalid;

    // Members used for gaze bubble (filtered gaze visualization):
    private bool _hasHistoricPoint;
    private Vector3 _historicPoint;

    [Range(0.1f, 1.0f), Tooltip("How heavy filtering to apply to gaze point bubble movements. 0.1f is most responsive, 1.0f is least responsive.")]
    public float FilterSmoothingFactor = 0.15f;

    private float catPawnHeight;

    public enum CatState { LOOKING, PREPARATION, ATTACKS, AT_PLACE, FINISHED };
    private CatState currentState;

    private float currentTime;

    private float attackSpeed;
    private Vector3 initialAttackPos;

    void Awake()
    {
        currentState = CatState.LOOKING;
    }

    // Use this for initialization
    void Start ()
    {
        GazePoint gazePoint = EyeTracking.GetGazePoint();
        if (gazePoint.IsWithinScreenBounds) lastGazePoint = gazePoint;

        Vector3 position = transform.position;
        position.z = VISUALIZATION_DISTANCE;
        transform.position = position;

        catPawnHeight = transform.position.y;
    }
	
	// Update is called once per frame
	void Update ()
    {
        StateMachine();
    }
    
    void LateUpdate ()
    {
        if ( currentState == CatState.LOOKING )
        {
            Vector3 targetPosition = (Smoothify(ProjectToPlaneInWorld(lastGazePoint)));
            if (Mathf.Abs((targetPosition.y - transform.position.y)) <= THRESHOLD) targetPosition.y = transform.position.y;

            Vector3 newPosition = (targetPosition - transform.position) * MOVING_SPEED * Time.deltaTime;
            newPosition.y = 0;
            transform.position += newPosition;

            GazePoint gazePoint = EyeTracking.GetGazePoint();

            if (gazePoint.SequentialId > lastGazePoint.SequentialId && gazePoint.IsWithinScreenBounds)
            {
                lastGazePoint = gazePoint;
            }
        }
    }

    private Vector3 ProjectToPlaneInWorld(GazePoint gazePoint)
    {
        Vector3 gazeOnScreen = gazePoint.Screen;
        gazeOnScreen += (transform.forward * VISUALIZATION_DISTANCE);
        return Camera.main.ScreenToWorldPoint(gazeOnScreen);
    }

    private Vector3 Smoothify(Vector3 point)
    {
        if (!_hasHistoricPoint)
        {
            _historicPoint = point;
            _hasHistoricPoint = true;
        }

        var smoothedPoint = new Vector3(
            point.x * (1.0f - FilterSmoothingFactor) + _historicPoint.x * FilterSmoothingFactor,
            point.y * (1.0f - FilterSmoothingFactor) + _historicPoint.y * FilterSmoothingFactor,
            point.z * (1.0f - FilterSmoothingFactor) + _historicPoint.z * FilterSmoothingFactor);

        _historicPoint = smoothedPoint;

        return smoothedPoint;
    }

    Vector3 targetAttackPos;
    private void StateMachine()
    {
        switch (currentState)
        {
            case CatState.LOOKING:

                currentTime += Time.deltaTime;
                if (currentTime >= LOOKING_TIME)
                {
                    currentTime = 0f;
                    currentState = CatState.PREPARATION;
                }

            break;

            case CatState.PREPARATION:

                currentTime += Time.deltaTime;
                if (currentTime >= PREP_TIME)
                {
                    currentTime = 0f;
                    currentState = CatState.ATTACKS;

                    float targetHeight = Random.Range(CAT_MIN_Y_OFFSET,  CAT_MAX_Y_OFFSET);
                    initialAttackPos = transform.position;
                    targetAttackPos = transform.position;
                    targetAttackPos.y += targetHeight;

                    attackSpeed = Mathf.Abs(targetAttackPos.y - initialAttackPos.y) / ATTACK_TIME;
                }

           break;

            case CatState.ATTACKS:

                currentTime += Time.deltaTime;

                //transform.position += initialAttackPos * attackSpeed * currentTime;
                transform.position = Vector3.Lerp(initialAttackPos, targetAttackPos, currentTime/ATTACK_TIME);

                if(currentTime >= ATTACK_TIME)
                {
                    currentTime = 0f;
                    currentState = CatState.AT_PLACE;
                }


            break;

            case CatState.AT_PLACE:

                currentTime += Time.deltaTime;

                if (currentTime >= AT_PLACE_TIME)
                {
                    currentTime = 0f;
                    currentState = CatState.LOOKING;
                    Vector3 backPos = transform.position;
                    backPos.y = catPawnHeight;
                    transform.position = backPos;
                }


                break;

            case CatState.FINISHED:
                break;

            default:
                return;
        }
    }
}
