﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.EyeTracking;

[RequireComponent(typeof(GazeAware))]
public class SpinOnGaze : MonoBehaviour {

    private GazeAware _gazeAware;

	// Use this for initialization
	void Start () {
        _gazeAware = GetComponent<GazeAware>();

    }
	
	// Update is called once per frame
	void Update () {
		if(_gazeAware.HasGazeFocus)
        {
            transform.Rotate(Vector3.forward);
        }            
	}
}
