﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SeventyOneSquared;

public class ToastBehavior : MonoBehaviour {

    public GameObject DEATH_EMISSION_PREFAB;

    private ToasterBehavior toasterBehavior;

    private bool timedout;

    void Awake()
    {
        timedout = false;
    }

    // Use this for initialization
    void Start()
    {
        toasterBehavior = FindObjectOfType(typeof(ToasterBehavior)) as ToasterBehavior;

        GazeTimeCounter.OnTimeout += Timeout;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnDestroy()
    {
        GazeTimeCounter.OnTimeout -= Timeout;
    }

    private void Timeout(GameObject self)
    {
        if ((!timedout) && self.Equals(this.gameObject))
        {
            timedout = true;
            toasterBehavior.CharacterDied(this.gameObject);
            Destroy(self);

            GameObject emitterObject = Instantiate(DEATH_EMISSION_PREFAB) as GameObject;
            emitterObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 2);

            PDUnity emitter = emitterObject.GetComponent<PDUnity>();
            emitter.Running = true;
        }
    }
}
