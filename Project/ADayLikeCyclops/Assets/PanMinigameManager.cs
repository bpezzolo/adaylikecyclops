﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanMinigameManager : MonoBehaviour {

    private IEnumerator coroutine;

    private MiniGameManager miniGameManager;

    // Use this for initialization
    void Start ()
    {
        miniGameManager = FindObjectOfType(typeof(MiniGameManager)) as MiniGameManager;

        MiniGameManager.OnEndGame += EndPanMiniGame;
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    void OnDestroy()
    {
        MiniGameManager.OnEndGame -= EndPanMiniGame;
    }

    void EndPanMiniGame (bool didWin)
    {
        if (didWin)
        {
            coroutine = WaitAndChangeScene(3.0f);
            StartCoroutine(coroutine);
        }
    }

    // every 2 seconds perform the print()
    private IEnumerator WaitAndChangeScene(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        miniGameManager.ChangeScene(false);
    }

}
