﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Spine.Unity;
using Spine;
using SeventyOneSquared;

public class PanBehavior : MonoBehaviour {

    public GameObject PAN_SKELETON;

    public Animator GIRL_ANIMATOR;

    public PDUnity POPCORN_EMITTER;

    private MiniGameManager miniGameManager;

    private GazeTimeCounter gazePan;

    private float currentProgress;

    private float inflateAnimationDuration;

    private SkeletonAnimation panSkeletonAnimation;

    private bool finished;

    void Awake ()
    {
        currentProgress = 0f;

        finished = false;
    }

    // Use this for initialization
    void Start () {
        miniGameManager = FindObjectOfType(typeof(MiniGameManager)) as MiniGameManager;

        if (!miniGameManager)
        {
            Debug.LogError("Where is the miniGameManager?");
        }

        panSkeletonAnimation = PAN_SKELETON.GetComponent<SkeletonAnimation>();

        panSkeletonAnimation.state.ClearTracks();
        
        TrackEntry track = panSkeletonAnimation.state.SetAnimation(0, "inflate", false);
        inflateAnimationDuration = track.AnimationEnd - track.AnimationStart;
        track.timeScale = 0f;
    
        gazePan = GetComponentInChildren<GazeTimeCounter>() as GazeTimeCounter;

        MiniGameManager.OnEndGame += MakeEndGameAnimation;

        GazeTimeCounter.OnTimeup += EndGame;

        UpdatePan();
    }
	
	// Update is called once per frame
	void Update () {
        if (!finished)
        {
            UpdatePan();
        }
    }

    void OnDestroy()
    {
        MiniGameManager.OnEndGame -= MakeEndGameAnimation;
        GazeTimeCounter.OnTimeup -= EndGame;
    }

    void UpdatePan()
    {

        TrackEntry track = panSkeletonAnimation.state.GetCurrent(0);
        float currentTrackTime = track.trackTime;
        float newTrackTime = inflateAnimationDuration * gazePan.GetProgress();
        track.trackTime = newTrackTime;

        if (currentTrackTime != newTrackTime)
        {
            GIRL_ANIMATOR.SetBool("MakingProgress", newTrackTime > currentTrackTime);
        }
    }

    void EndGame(GameObject sel)
    {
        if (!finished)
        {
            finished = true;
            miniGameManager.EndGame(false);
            miniGameManager.ChangeScene(false);
        }
    }

    void MakeEndGameAnimation(bool didWin)
    {
        finished = true;
        if (didWin)
        {
            GIRL_ANIMATOR.SetBool("MakingProgress", true);

            StartCoroutine(FinalAnimationCoroutine());
        }
        else
        {
            GIRL_ANIMATOR.SetBool("MakingProgress", false);
        }
    }

    private IEnumerator FinalAnimationCoroutine()
    {
        GIRL_ANIMATOR.SetBool("MakingProgress", true);

        TrackEntry track = panSkeletonAnimation.state.SetAnimation(0, "explosion", false);
        track.timeScale = 1f;
        
        yield return new WaitForSeconds(0.3f);

        POPCORN_EMITTER.Running = true;
        POPCORN_EMITTER.AutoLoop = true;
    }
}
